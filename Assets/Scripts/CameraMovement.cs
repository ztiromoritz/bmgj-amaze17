﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public Transform target;
	public float lerpSpeed = 1f;
	private Vector3 offsetPosition;

	// Use this for initialization
	void Start () {
		offsetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 wantedPostion = target.position-offsetPosition;

		transform.position = Vector3.Lerp(transform.position, wantedPostion, lerpSpeed);
	}
}
