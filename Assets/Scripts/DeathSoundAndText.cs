﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSoundAndText : MonoBehaviour {

	public AudioClip intro;
	public AudioClip collectable;
	public AudioClip[] deathSounds;
	public string[] deathTexts;

	private AudioSource aSource;

	// Use this for initialization
	void Start ()
    {
		aSource = this.GetComponent<AudioSource>();
	}

    public void playIntro()
    {
        aSource.PlayOneShot(intro);
    }

	public void playRandomDeathSound()
	{
		int index = Random.Range(0, deathSounds.Length-1);
		aSource.PlayOneShot(deathSounds[index]);

	}

	public void playCollectingSound()
	{
		aSource.PlayOneShot(collectable);
	}
}
