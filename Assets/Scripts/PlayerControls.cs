﻿using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

    public DeathSoundAndText soundAndText;
    public Text glassPartCountText;
    public Text deathCountText;
    public Text screenInfoText;
    public float infoTextDisplayTime = 5f;

    public Vector3 startPosition = new Vector3(0,1,0);
	public float playerSpeed = 1.0f;
    
    private int glassPartsCollected = 0;
    private int deathCount = 0;
	private Rigidbody rBody;
	private HashSet<GameObject> visited;

    private float timer = 0;

	// Use this for initialization
	void Start () 
	{
        timer = infoTextDisplayTime;
		visited = new HashSet<GameObject>();
		rBody = this.GetComponent<Rigidbody>();
        soundAndText.playIntro();
        glassPartCountText.text = "YOU ARE BLIND LIKE A BAT...GO FIND YOUR GLASSES";
        screenInfoText.text = "GENERIC DEATH ZONES 2";
        deathCountText.text = "";
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            screenInfoText.text = "";
        }

		float x = playerSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
		float z = playerSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
		Vector3 direction = new Vector3(x, 0, z);
		rBody.AddForce(direction);

		if (direction.magnitude > 0) {
			transform.rotation = Quaternion.LookRotation(direction.normalized);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "GenericZoneOfDeath")
		{
            playerDeath();
		}
	}

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "Bar" && !visited.Contains(other.gameObject))
		{
			visited.Add(other.gameObject);
            playerCollectGlassPart();
        }
	}

    void playerDeath()
    {
        deathCount++;
        deathCountText.text = "YOU DIED TO DEATH... THE " + deathCount + ". TIME";
        soundAndText.playRandomDeathSound();
        playerReset();
    }

    void playerCollectGlassPart()
    {
        glassPartsCollected++;
        glassPartCountText.text = "YOU HAVE FOUND " + glassPartsCollected + " PART(S) OF WHAT YOU CALLED GLASSES";
        soundAndText.playCollectingSound();
        
        if (glassPartsCollected == 3)
        {
            restartGame();
        }

    }

    void playerReset()
    {
        transform.position = startPosition;
        glassPartsCollected = 0;
        glassPartCountText.text = "YOU ARE BLIND LIKE A BAT...GO FIND YOUR GLASSES";
        visited.Clear();
    }

    void restartGame()
    {
        timer = infoTextDisplayTime;
        screenInfoText.text = "YOU MADE IT! YOU CAN NOW GO THROUGH THE STRUGGLE AGAIN :D";

        deathCount = 0;
        deathCountText.text = "";
        playerReset();
    }
}
