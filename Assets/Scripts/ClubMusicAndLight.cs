﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClubMusicAndLight : MonoBehaviour {

	public Color color_1;
	public Color color_2;
	public float colorChangeSpeed = 1f;

	public Light clubLight;
	private Color currentColor;

	// Use this for initialization
	void Start () {

		currentColor = color_1;
        clubLight.color = currentColor;
	}
	
	// Update is called once per frame
	void Update () {
		float t = Mathf.PingPong(Time.time, colorChangeSpeed) / colorChangeSpeed;
		currentColor = Color.Lerp(color_1, color_2, t);
        clubLight.color = currentColor;

	}
}
